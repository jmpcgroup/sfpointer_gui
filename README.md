# sfpointer_gui



## Getting started

In order to use the app, follow the following steps:

1.- Clone the repository.<br>
2.- Open the .Rproj located in sfpointerApp folder.<br>
3.- Install the required libraries:<br>

```
install.packages(shiny) 
install.packages(bigmemory) 
install.packages(ggplot2) 
install.packages(ggpubr) 
install.packages(reshape2) 
install.packages(dplyr) 
install.packages(kableExtra) 
install.packages(shinycssloaders) 
install.packages(htmlTable) 
install.packages(RColorBrewer) 
install.packages(bslib) 
install.packages(shinyWidgets) 
install.packages(readxl) 
install.packages(rapportools) 
install.packages(scales) 
install.packages(shinymaterial) 
install.packages(htmlwidgets) 
install.packages(DT)
BiocManager::install("biomaRt")
install.packages(openxlsx) 
install.packages(Matrix) 
install.packages(speedglm) 
install.packages(shinythemes) 
```

4.- Oppen the app.R file and *Run App*<br>

