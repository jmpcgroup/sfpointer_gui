---
title: "Untitled"
author: "Juan A. Ferrer-Bonsoms"
date: "11 de mayo de 2022"
output:
  html_document:
  css: style.css
---


<p style="text-align:center;"><img src="logo.png" alt="Welcome" style="width: 25%"/></p>


<center>

#### *"SFPointer: Prediction of RBPS driving aberrant splicing in cancer"*
</center>


<br>

## Welcome!

#### This website is the final contribution of the SFPointer proposal that provides a convenient framework to consult the results of our work. We termed this algorithm as (<b><font color="#20a6a6">SF</font></b>(Splicing Factor) + <b><font color="#494c4f">pointer</font></b>) as our goal is to predict which Splicing Factor (or other RNA Binding Proteins) is  driving the aberrant alternative splicing in different cancer types.
<br>
#### The initial data and source code of the model are available at <a href='https://github.com/clobatofern/SFPointer_testPipeline' target='_blank'> github </a>.
<br>
#### This website has a main panel:
#### <b>SF APP: users can select the type of cancer and download the results.</b>
<br>

#### Other panels of the app describes the motivation of this project (About), the current version of the app (Version), the future lines derived from this work (Future Lines), and the methodology of each approach (Methods).

<br>
<center>
<br>
#### **The data is already loaded in the app and ready for analysis**
<br>
</center>

## 1. SF APP
### 1.1 Cancer type and statistic selection
#### Main panel of the website is to consult which RBP is driving aberrant splicing in a selected type of cancer. Therefore, the first step of a user is to select the type of cancer, the approach to use (Fisher, Poibin or Wilcoxon), and the different features of these approaches: use the top 1000 significant events or use the events with a significance lower than 0.01:
<br>
<p style="text-align:center;"><img src="select_type_cancer.png" alt="Select Cancer type and the statistic approach"/></p>
### RUN/UPDATE button
#### The corresponding table of the prediction will be updated when the users pushes this buttom (depicted in the picture above)

### 1.2 RBP related to selected Cancer
#### This is "the main panel of the main panel". In this tab, the app display firstly a ranked table with the predicted RBP, the events of the RBP selected and a picture of the selected event.
<br>
#### **RBP table**
<br>
<p style="text-align:center;"><img src="RBP_table.png" alt="RBP prediction for AML using the Fisher approach"/></p>
#### This is the ranked table with the predicted RBP. That is, the first row corresponds to the RBP that our algorithm predicts as the most likely to cause aberrant splicing. In this table, for each RBP there is a link that redirects you to the GENECARDS page, where you can consult information on each RBP. On the other hand, you can select an RBP (second column) to show which particular splicing events are modified by the selected RBP (shown in the following table). In addition, this table can be downloaded
<br>
#### **Events regulated by selected RBP**
<br>
<p style="text-align:center;"><img src="events_reg.png" alt="Events regulated by RBM3"/></p>
#### This table shows the information of the events regulated by the selected RBP. Specifically, it shows the name of the event, the p-value corresponding to the statistical analysis (performed with EventPointer) and the ranking they have (with respect to all events) in that analysis. A user in this table can select an event to view its structure. In addition, this table can be downloaded.
<br>

#### **Plot structure of selected event (beta version)**
<br>
<p style="text-align:center;"><img src="structure_event_1.png" alt="AS event TBC1D20_5: retained intron in RBC1D20 gen"/></p>
#### This graph shows the structure of the transcripts of the selected gene. In red is marked path 1 of the event and in blue is marked path 2 of the event (path 1 and path 2 refer to the alternative sequence that produces the splicing event). This figure can also be downloaded.
<br>



### 1.3 Events differently spliced
<br>
#### **Events table**
<br>
<p style="text-align:center;"><img src="events_table.png" alt="RBP prediction for AML using the Fisher approach"/></p>
#### This panel shows a table with the statistical result for all splicing events. Specifically, the 100 most significant ones are shown. A user can select an event to display its structure.
<br>
#### **Plot structure of selected event (beta version)**
<br>
<p style="text-align:center;"><img src="structure_event_2.png" alt=" "/></p>
#### This graph shows the structure of the transcripts of the selected gene. In red is marked path 1 of the event and in blue is marked path 2 of the event (path 1 and path 2 refer to the alternative sequence that produces the splicing event). This figure can also be downloaded.
<br>
