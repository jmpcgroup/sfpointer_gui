######### loading libreries #######
library(shiny) #cran
library(bigmemory) #cran
library(ggplot2) #cran
library(ggpubr) #cran
library(reshape2) #cran
library(dplyr) #cran
library(kableExtra) #cran
library(shinycssloaders) #cran
library(htmlTable) #cran
library(RColorBrewer) #cran
library(bslib) #cran
library(shinyWidgets) #cran
library(readxl) #cran
library(rapportools) #cran
library(scales) #cran
library(shinymaterial) #cran
library(htmlwidgets) #cran
library(DT)
library(biomaRt)
library(openxlsx) #cran
library(Matrix) #cran
library(speedglm) #cran
library(shinythemes) # cran

######### loading data #######
load("./input_data/ExS_Postar3.RData")
remove_badrbp <- which(colnames(ExS_Postar3)=="RBP_occupancy")
ExS_TCGA <- ExS_Postar3[,-remove_badrbp]
rm(ExS_Postar3)

milista_RBPs <- colnames(ExS_TCGA)

load("./input_data/ExS_Postar3_hg19.RData")
ExS_target <- ExS_Postar3_hg19$ExS
rm(ExS_Postar3_hg19)
remove_badrbp <- which(colnames(ExS_target)=="RBP_occupancy")
ExS_target <- ExS_target[,-remove_badrbp]


load("./input_data/getBM.RData")
getBM_TCGA <- getBM
rm(getBM)
load("./input_data/getBM_Target.RData")
colnames(getBM_Target) <- c("Gene_ID","Gene_name")


#infoevents TCGA
infoevents_TCGA <- read.delim(file="./input_data/EventsFound_gencode24.txt",
                          header = T,sep = "\t")
infoevents_TCGA$ID <- paste0(infoevents_TCGA$GeneName,"_",infoevents_TCGA$EventNumber)

# load("./input_data/EventsXtrans_gc24.RData")
# SG_list_TCGA <- EventsXtrans_gc24$SG_List
# save(SG_list_TCGA,file="./input_data/SG_list_TCGA.RData")
load("./input_data/SG_list_TCGA.RData")

#infoevents TARGET
infoevents_TARGET <- read.delim(file="./input_data/EventsFound_Ensembl_GRCh37_v65.txt",
                              header = T,sep = "\t")
infoevents_TARGET$ID <- paste0(infoevents_TARGET$GeneName,"_",infoevents_TARGET$EventNumber)

# load("./input_data/EventXtrans_grch37_v65.RData")
# SG_list_TARGET <-  EventXtrans_grch37_v65$SG_List
# save(SG_list_TARGET,file="./input_data/SG_list_TARGET.RData")
load("./input_data/SG_list_TARGET.RData")
missamples <- dir("./input_data/",pattern = "boots*")
tipodecancer <- gsub(".RData","",gsub("bootstrap_","",missamples))

source("./input_data/internal_sf_prediction.R")
source("./input_data/SF_Prediction_cesar.R")
source("./input_data/draw_event.R")

# Tablas indices -> Mejor cargarlo y poner el código en otro archivo?

# Mi_tabla_index_mitad1 <- data.frame(
#   "Normal inicio" = c(1, 434,1659,2181,2378, 2941,3033),
#   "Normal fin" = c(19,546,1699,2193,2421,2966,3104),
#   "Tumoral inicio" = c(20,547,1700,2194,2422,2967,3105),
#   "Tumoral fin" = c(433,1658,2180,2377,2940,3032,3645)
# )
# 
# rownames(Mi_tabla_index_mitad1) <- c("BLCA","BRCA","COAD","ESCA","HNSC","KICH","KIRC")

#mitad 2

# Mi_tabla_index_mitad2 <- data.frame(
#   "Normal inicio" = c(1, 323, 744, 1342, 1895, 2448, 2624, 3077, 3641),
#   "Normal fin" = c(32, 372, 802, 1392, 1946, 2457, 2660, 3135, 3675),
#   "Tumoral inicio" = c(33, 373,803,1393,1947,2458,2661, 3136, 3676),
#   "Tumoral fin" = c(322, 743,1341,1894, 2447, 2623, 3076, 3640, 4228)
# )

# rownames(Mi_tabla_index_mitad2) <- c("KIRP","LIHC","LUAD","LUSC","PRAD","READ","STAD","THCA","UCEC")

#### Big Memory ####

##### Mitad 1 #####
# ReadPath1 <- dget("./../../Paper_SF_pointer_2_0/TCGA/BigMemory/Path1_Exp_mitad1.desc")
# ReadPath1@description$dirname <- "./../../Paper_SF_pointer_2_0/TCGA/BigMemory/"
# Path1_Exp <- attach.big.matrix(ReadPath1)
# 
# ReadPath2 <- dget("./../../Paper_SF_pointer_2_0/TCGA/BigMemory/Path2_Exp_mitad1.desc")
# ReadPath2@description$dirname <- "./../../Paper_SF_pointer_2_0/TCGA/BigMemory/"
# Path2_Exp <- attach.big.matrix(ReadPath2)
# dim(Path2_Exp)

##### Mitad 2 #####
# ReadPath1_mitad2 <- dget("./../../Paper_SF_pointer_2_0/TCGA/BigMemory/Path1_Exp_mitad2.desc")
# ReadPath1_mitad2@description$dirname <- "./../../Paper_SF_pointer_2_0/TCGA/BigMemory/"
# Path1_Exp_mitad2 <- attach.big.matrix(ReadPath1_mitad2)
# dim(Path1_Exp_mitad2)
# 
# ReadPath2_mitad2 <- dget("./../../Paper_SF_pointer_2_0/TCGA/BigMemory/Path2_Exp_mitad2.desc")
# ReadPath2_mitad2@description$dirname <- "./../../Paper_SF_pointer_2_0/TCGA/BigMemory/"
# Path2_Exp_mitad2 <- attach.big.matrix(ReadPath2_mitad2)
# dim(Path2_Exp_mitad2)

#color ana y ane: 5C6BC0 y 7986CB

######### ********************** ui ********************** #######
ui <- tagList(
  tags$head(
    tags$style(
      HTML("<img src='logo.png' width='40px' height='30px'> ")
    )
  ),
  navbarPage(
    theme = shinytheme(theme = "spacelab"),
    title = div(
      img(
        src = 'logo.png',
        height = 30,
        width = 40
      ),
      ""
    ),
  tabPanel('Overview',
           fluidPage(
             includeMarkdown("help/sfpointer_gui_help.Rmd")
           )
  ),
  tabPanel('SF APP',
    fluidPage(titlePanel(h1("RBP PREDICTION IN TCGA AND TARGET",style={'color:#20a6a6;'})),

                # sidebarLayout(
              fluidRow(
                  sidebarPanel(

                    ###### Tipo de cancer #####
                    prettyRadioButtons("tipodecancer", label = HTML('<FONT color= #20a6a6><FONT size="5pt">TYPE OF CANCER</FONT></FONT><br>'),
                                       choices = tipodecancer,
                                       status = "primary",
                                       fill = F,
                                       animation = "smooth"
                    ),
                    hr(),

                    fluidRow(column(3, verbatimTextOutput("cancer"))),

                    ###### Tipo de estadistica ######
                    prettyRadioButtons("estadistica",label=HTML('<FONT color= #20a6a6><FONT size="5pt">STATISTIC</FONT></FONT><br>'),
                                       choices = list("Fisher" = "Fisher",
                                                      "Poisson Binomial" = "PoiBin",
                                                      "Wilcoxon" = "Wilcoxon"),
                                       selected = "Fisher",
                                       status = "primary",
                                       fill = F,
                                       animation = "smooth"
                    ),

                    hr(),
                    fluidRow(column(3, verbatimTextOutput("estadistica"))),

                    ###### Tipo de selección ######

                    prettyRadioButtons("seleccion",label=HTML('<FONT color= #20a6a6><FONT size="5pt">SELECTION</FONT></FONT><br>'),
                                       choices = list("Best 1000 events" = "1000",
                                                      "Significance (0.01)" = "0.01"),
                                       status = "primary",
                                       fill = F,
                                       animation = "smooth"

                    ),

                    hr(),
                    fluidRow(column(3, verbatimTextOutput("seleccion"))),

                    ###### Botón de run ######
                    # actionButton("run", label = "Run", style="color: #fff; background-color: #20a6a6; border-color: #20a6a6"),
                    actionBttn(inputId = "updateresults",size = "lg",label = "Run/Update",
                               style = "gradient",color = "success",block = TRUE,no_outline = FALSE),
                    hr(),
                    fluidRow(column(2, verbatimTextOutput("Run"))),
                    width=3
                  ),
                  column(9,
                         
                  ###### Main Panel ######
                  mainPanel(
                    ###### ****** FIRST PANEL ******* ######
                    tabsetPanel(
                      ######## TABLA 1 CON RBP RELATED TO SELECTED CANCER #############
                      tabPanel(h5("RBP related to selected Cancer",style={'color:#615654;'}),
                               h4("RBP table",style={'color:#20a6a6;'}),
                               p("This table shows the ranking of RBPS predicted to be driving the splicing in the selected Cancer"),
                               # withSpinner(htmlOutput("tabla1", align="center")),
                               #tags$div(id="tabla1",class='shiny-input-radiogroup', DT::dataTableOutput('foo',width = "100%", height = "auto"),style = "width:950px"),
                               # tags$div(id="tabla1",class='shiny-input-radiogroup', DT::dataTableOutput('foo', height = "auto"),style = "width:950px"),
                               tags$div(id="tabla1",class='shiny-input-radiogroup', DT::dataTableOutput('foo', height = "auto")),
                               downloadButton(outputId = "download_tabla1", label = "Download"),
                               hr(),
                               ######## TABLA 2 events related to selected RBP #############
                               h4("Events regulated by selected RBP",style={'color:#20a6a6;'}),
                               p("This table shows the alternative splicing events regulated by the RBP selected in the table above"),
                               #withSpinner(htmlOutput("tablaCancer", align="center"))),
                               #tags$div(id="tablaCancer",DT::dataTableOutput('foo_3', width = "100%", height = "auto"),style = "width:700px; height:500px; overflow-y: scroll;overflow-x: scroll"),
                               tags$div(id="tablaCancer",class='shiny-input-radiogroup',DT::dataTableOutput('foo_3',width = "100%", height = "auto")),
                               downloadButton(outputId = "download_tablaCancer", label = "Download"),
                               hr(),
                               h4("Plot structure of selected event (beta version)",style={'color:#20a6a6;'}),
                               plotOutput("plot_panel1"),
                               downloadButton(outputId = "download_plot_panel1", label = "Download plot")
                               ),

                      ###### ****** SECOND PANEL ******* ######
                      tabPanel(h5("Events differently spliced",style={'color:#615654;'}),
                               h4("Events table",style={'color:#20a6a6;'}),
                               p("This table shows the p-value and the increment of PSI for all the events"),
                               #withSpinner(htmlOutput("tabla2", align="center")),
                               #tags$div(id="tabla2",class='shiny-input-radiogroup',DT::dataTableOutput('foo_2', width = "100%", height = "auto"), style = "width:700px; height:500px; overflow-y: scroll;overflow-x: scroll;"),
                               tags$div(id="tabla2",class='shiny-input-radiogroup', DT::dataTableOutput('foo_2')),
                               downloadButton(outputId = "download_tabla_panel2", label = "Download"),
                               hr(),
                               h4("Plot structure of selected event (beta version)",style={'color:#20a6a6;'}),
                               plotOutput("plot"),
                               downloadButton(outputId = "download_plot", label = "Download plot")

                      ),
                      ###### ****** THIRD PANEL ******* ######
                      tabPanel(
                        h5("Survival analysis",style={'color:#B0BEC5;'}),
                        p("This panel will show a Study of RBP based on survival analysis.
                        \nIt will be available in the next version of the APP.")#,
                        # h4("Histogram",style={'color:#20a6a6;'}),
                        # p("A Cox regression of all the events using its PSI value as a variable will be run.
                        #   \nA histogram of the corresponding p-vales will be displayed"),
                        # plotOutput("hist"),
                        # hr(),
                        # h4("Information about events",style={'color:#20a6a6;'}),
                        # p("A table with the results of all the cox regression will be available."),
                        # tags$div(id="tabla_supervivencia",DT::dataTableOutput('foo_4',height = "auto")),
                        # hr(),
                        # h4("RBP Prediction",style={'color:#20a6a6;'}),
                        # p("A prediction of which RBP is modifying the events more related to survival will be available.
                        #   \nTherefore, this table will give us an idea of which RBP is more related to survival."),
                        # tags$div(id="tabla5", DT::dataTableOutput('foo_5'))
                      )

                    )
                  )
                  )
              )
                # )
            )
  ),
  tabPanel('About',
           img(src='logo.png', width = "200", align = "right"),
           h2("Motivation:"),
           p("Alternative Splicing (AS) is a post-transcriptional process by which a single RNA can lead to different mRNA and, in some cases, several proteins. Various processes (probably many of them yet to be discovered) are involved in  the regulation of alternative splicing. This work focuses on the regulation by RNA-binding proteins (RBPs).  In addition to splicing regulation, these proteins are related to cancer prognosis and are emerging therapeutic  targets for cancer treatment. The specific relationship between RBPs and AS is still unknown.  "),
           h2("Results:"),
           p("In this work, we have integrated several databases of CLIP-seq experiments with an  algorithm that detects differential splicing events to discover RBPs that are especially  enriched. We have applied this methodology to the TCGA and TARGET database, analyzing a total  of 19 types of cancer. Thanks to this app, the results are accessible to the scientific community.")
  ),
  
  tabPanel('Version',
           img(src='logo.png', width = "200", align = "right"),
           h4("Version 1.0 (May 13th, 2022):"),
           p("Fisher, PoiBin and Wilcoxon approaches with default values are available for TCGA and TARGET"),
           p("User can download the table of the results and a picture of the structure of the AS events (beta version)")
          
  ),
  
  tabPanel('Future Lines',
           img(src='logo.png', width = "200", align = "right"),
           h4("On going future lines"),
           p("Users will be able to modify the features of Fisher, PoiBin and Wilcoxon approaches."),
           p("User can use the GSEA approach."),
           p("Survival analysis: impact of RBP on survival.")
           
  ),
  
  tabPanel('Methods',
           img(src='logo.png', width = "200", align = "right"),
           h4("Fisher"),
           p("Fisher’s Exact Test was already described in a former version fo this algorithm. 
The Fisher test is based on a hypergeometric variable to
calculate the probability of seeing an anormal number of
events that are differentially spliced and regulated by a RBP."),
           p("This application can be translated to splicing factors. It was intended to calculate 
the probability that a significant SF will emerge if the 1000 best events are selected,
how many of them are regulated by a certain SF, and if there are many more of the events
expected by chance. The RBPs were ranked by p-value.  "),
           p("Furthermore, we will include in the next version of the app a novel  options to select the relevant splicing events:
select the splicing events with a Local False Discovery Rate under a threshold
(we used Local FDRthrs=0.1). "),
           h4("Poison Binomial (PoiBin)"),
           p("From ExS matrix we compute the probability of a specific event being regulated
by a specific Splicing Factor.  we developed 
our own algorithm to solve this problem,
specifically, we solved it using a logistic regression. 
We called this algorithm  Rediscover and its available as an R package in the CRAN repository."),
           p("Then, given this probabilities, we calculate the probability of observe an anormal number of events that
are differentially spliced and regulated by a RBP.
This probability is computed with the Poisson Binomial Distribution. "),
           h4("Wilcoxon"),
           p("The Wilcoxon test can also be applied to perform an enrichment analysis. 
A Wilcoxon test is a non-parametric test that compares the medians of two datasets.
In this case, the distributions to be compared are the p.values of the event 
annotated with an RBP binding site with the p.values of the events 
not annotated with the same RBP binding site."),
           p("The Wilcoxon test does not either require setting a
threshold on the p-values. Our implementation (that uses sparse matrices and linear algebra)
is very fast as will be shown in the results. "),
           h4("GSEA"),
           p("GSEA is a non-parametric test based on the Kolmogorov-Smirnov test that compares
the distributions of a variable (usually a p.value, but other possibilities are also valid)
between the analytes (usually genes) that have a characteristic (usually a GO annotation)
and those that do not have the characteristic. The application to RBP analysis is straightforward. 
The variable is the p.value of the splicing event and the characteristic is the presence or absence
of an annotated RBP binding site in the neighborhood of the event. One of the advantages of GSEA is 
that it does not require to set a threshold on the p.value to state which are the significant events."),
           p("We have used the R-package fgsea. It allows to “quickly and accurately calculate arbitrarily 
low GSEA P-values for a collection of gene sets” by using an adaptive
multi-level split Monte-Carlo scheme.  Despite being a very fast implementation,
it is still slower than any of the other implemented methods. This is why, in the current version
of the app is not allowed (it will be in the next version). ")
           
  )
  
  ),
  footer=p(hr(),p("ShinyApp created by ", strong("Ane San Martín"), ", ",strong("Ana Anorbe"), ", ",strong("Juan A. Ferrer-Bonsoms"),", and",strong("Angel Rubio"),align="center",width=4),
           p(("University of Navarra, Tecnun. Biomedical Engineering and Sciences Department"),align="center",width=4),
           p(("Authors: Marian Gimeno, Cesar Lobato, Ane San Martian, Ana Anorbe, Juan A. Ferrer-Bonsoms, and Angel Rubio"),align="center",width=4),
           p(("Contact: "),a("arubio@tecnun.es"),", ",a("jafhernandez@tecnun.es"),align="center",width=4))
  
)

######### *******************  SERVER ************************ #######
server <- function(input, output, session) {
  
  ####### ********* input datos necesarios Botón de run ********* #######
  Run <- eventReactive(ignoreInit = FALSE,input$updateresults,{
    # browser()
    # input <- list()
    # input$tipodecancer <- "AML"
    # input$estadistica <- "Wilcoxon"
    if(input$tipodecancer %in% c("AML","WT","RT")){
      ExS <- ExS_target
      getBM <- getBM_Target
      SG_list <- SG_list_TARGET
      infoevents <- infoevents_TARGET
    }else{
      ExS <- ExS_TCGA
      getBM <- getBM_TCGA
      SG_list <- SG_list_TCGA
      infoevents <- infoevents_TCGA
    }
    ####### CARGAR DATOS NECESARIOS #######
    # command_1 <- paste0('load("./../../Paper_SF_pointer_2_0/TCGA/results/sf_prediction_',input$tipodecancer,'_',input$estadistica,'_',input$seleccion,'.RData")')
    command_1 <- paste0('load("./input_data/bootstrap_',input$tipodecancer,'.RData")')
    eval(parse(text=command_1))
    #Usamos todas las tablas sin filtrar
    
    # input$estadistica <- "Fisher"
    command_4 <- paste0('mitabla <- SF_Prediction_cesar(P_value_PSI = bootstrap_',input$tipodecancer,'$Pvalues,ExS = ExS,nSel = 1000,significance = NULL,method = "',input$estadistica,'")[[1]]')
    # command_4<- paste0("mitabla <- head(sf_prediction_",input$tipodecancer,'_',input$estadistica,'_',input$seleccion,"[[1]],n=10)")
    eval(parse(text=command_4))
    # head(mitabla)
    # browser()
    # command_2 <- paste0('load("./../../Paper_SF_pointer_2_0/TCGA/results/bootstrap_',input$tipodecancer,'.RData")')
    # eval(parse(text=command_2))
    command_3 <- paste0("Pvalues <- bootstrap_",input$tipodecancer,"$Pvalues")
    command_5 <- paste0("deltaPSI_",input$tipodecancer," <- bootstrap_",input$tipodecancer,"$deltaPSI")
    command_6 <- paste0("deltaPSI<- deltaPSI_",input$tipodecancer)
    
    command_7 <- paste0(c(command_3,command_5,command_6))
    
    # cat(command_7,sep="\n") 
    
    eval(parse(text = command_7))
    
    #### Nombres eventos ####
    
    
    
    miseventos <- rownames(ExS)
    
    # ppx <- match(gsub("\\..*","",miseventos),getBM_Target$ensembl_gene_id)
    # good_name_events <- paste0(as.vector(getBM$Gene_name)[ppx],"_",gsub(".*_","",miseventos))
    # event_names2 <- data.frame(former_name = rownames(ExS),good_name = good_name_events)
    # rownames(event_names2) <- event_names2[,1]
    
    if(input$tipodecancer %in% c("AML","WT","RT")){
      ppx <- match(gsub("_.*","",miseventos),getBM$Gene_ID)
      good_name_events <- paste0(as.vector(getBM$Gene_name)[ppx],"_",gsub(".*_","",miseventos))
      good_name_events[which(is.na(ppx))] <- miseventos[which(is.na(ppx))]
      event_names2 <- data.frame(former_name = rownames(ExS_target),good_name = good_name_events)
      rownames(event_names2) <- event_names2[,1]
    }else{
      ppx <- match(gsub("\\..*","",miseventos),getBM$Gene_ID)
      good_name_events <- paste0(as.vector(getBM$Gene_name)[ppx],"_",gsub(".*_","",miseventos))
      event_names2 <- data.frame(former_name = rownames(ExS),good_name = good_name_events)
      rownames(event_names2) <- event_names2[,1]
    }
    
    
    
    #### Supervivencia ####
    #browser()
    # filetoload <- paste0('./../../Paper_SF_pointer_2_0/TCGA/results_supervivencia/surv_',input$tipodecancer,'.RData')
    # if(exists(filetoload)){
    #   command1<-paste0('surv <- load("',filetoload,'")')
    #   eval(parse(text = command1))
    # }else{
    #   filetoload <- paste0('./../../Paper_SF_pointer_2_0/TCGA/results_supervivencia/surv_',tolower(input$tipodecancer),'.RData')
    #   command1<-paste0('surv <- load("',filetoload,'")')
    #   eval(parse(text = command1))
    # }
    # 
    # list(a =1)
    ##########
    
    # browser()
    # input$updateresults
    borrar <- c("RBP","N","d","n")
    mitabla_2 <- mitabla[ , !(names(mitabla) %in% borrar)]
    
    #tabla rbps
    nombres_rbp <- milista_RBPs
    nombres_rbp <- as.data.frame(nombres_rbp)
    # tipo_rbp <- milista_RBPs$TIPO
    # tipo_rbp <- as.data.frame(tipo_rbp)
    # rownames(tipo_rbp)<-nombres_rbp[,1]
    
    #mitabla
    misnombres <- rownames(mitabla_2)
    
    # tipo_rbp_cancer <- tipo_rbp[misnombres,]
    # tipo_rbp_cancer <- as.character(tipo_rbp_cancer)
    # tipo_rbp_cancer <- as.data.frame(tipo_rbp_cancer)
    # colnames(tipo_rbp_cancer)<- c("Type")
    
    #añadir link
    # link<-milista_RBPs$LINK
    link <- paste0("https://www.genecards.org/cgi-bin/carddisp.pl?gene=",nombres_rbp[,1])
    names(link) <- nombres_rbp[,1]
    
    link_rbp_cancer<- paste0("<a href='",link[misnombres],"' target='_blank'>",misnombres,"</a>")
    
    # mitabla_2 <- cbind(link_rbp_cancer, "", tipo_rbp_cancer, mitabla_2)
    mitabla_2 <- cbind(link_rbp_cancer, "", mitabla_2)
    for (i in seq_len(nrow(mitabla_2))) {
      mitabla_2[i, 2] = sprintf(
        '<input type="radio" name="%s" value="%s"/>',
        "tabla1", paste0("events_",i)
      )
    }
    
    # browser()
    
    if(input$estadistica=="Wilcoxon"){
      mitabla_2$Pvalue_hyp_PSI<- format(pnorm(mitabla_2$z.score,lower.tail = TRUE), scientific = TRUE)
      mitabla_2$Pvalue_hyp_PSI<- sub("^(.{1,4})....(.*)","\\1\\2\\3\\4\\5",mitabla_2$Pvalue_hyp_PSI)
      mitabla_2$z.score<- round(mitabla_2$z.score,2)
      mitabla_2 <- mitabla_2[,c(1,2,3,6,4,5)]
      colnames(mitabla_2)<-c(" ","Pick","NHits","Pvalues ","X","Zscore")
    }else{
      # mitabla$Pvalue_hyp_PSI<- round(mitabla$Pvalue_hyp_PSI,6)
      mitabla_2$Pvalue_hyp_PSI<- format(mitabla_2$Pvalue_hyp_PSI, scientific = TRUE)
      mitabla_2$Pvalue_hyp_PSI<- sub("^(.{1,4})....(.*)","\\1\\2\\3\\4\\5",mitabla_2$Pvalue_hyp_PSI)
      mitabla_2$Fc<- round(mitabla_2$Fc,2)
      
      # colnames(mitabla_2)<-c(" ","Pick","Type","NHits","Pvalues ","X","QH","FC")
      colnames(mitabla_2)<-c(" ","Pick","NHits","Pvalues ","X","QH","FC")  
    }
    
    
    
    
    ##### para tabla tab 2 
    # oox_1 <- order(Pvalues)[1:50]
    oox_1 <- order(Pvalues)
    Pvalues_3 <- Pvalues[oox_1,,drop=FALSE]
    # mitabla2<-head(Pvalues_3, n=50)
    mitabla2 <- Pvalues_3
    mitabla3 <- matrix(cbind(mitabla2,deltaPSI[oox_1]),ncol = 2)
    mitabla3 <- as.data.frame(mitabla3)
    mitabla3 <- mitabla3[order(mitabla3$V1), ]
    
    colnames(mitabla3) <- c("Pvalue","deltaPSI")
    rownames(mitabla3) <- rownames(mitabla2)
    mitabla3 <- as.data.frame(mitabla3)
    
    mitabla3$Pvalue<- format(mitabla3$Pvalue, scientific = TRUE)
    mitabla3$deltaPSI<- format(mitabla3$deltaPSI, scientific = TRUE)
    
    mitabla3 <- cbind("",mitabla3)
    
    colnames(mitabla3)<-c(" ","Pvalue","deltaPSI")
    
    
    #cambiar nombres de eventos
    eventos_finales2 <- rownames(mitabla3)
    gn_eventos2 <- event_names2[eventos_finales2,2]
    mitabla3<-cbind(gn_eventos2,mitabla3)
    colnames(mitabla3) <- c("Event","Pick","Pvalues", "deltaPSI")
    
    for (i in seq_len(nrow(mitabla3))) {
      mitabla3[i, 2] = sprintf(
        '<input type="radio" name="%s" value="%s"/>',
        "tabla2", mitabla3[i,1]
      )
    }
    
    mitabla3 <- cbind(mitabla3,"")
    for (i in seq_len(nrow(mitabla3))) {
      mitabla3[i, 5] = sprintf(
        '<input type="radio" name="%s" value="%s"/>',
        "tablaCancer", mitabla3[i,1]
      )
    }
    # browser()
    yyx <- match(rownames(mitabla3),infoevents$ID)
    mitabla3$EventType <- infoevents$EventType[yyx]
    
    
    ## output
    # browser()
    colnames(mitabla_2)[1] <- "RBP"
    list(
      good_name_events = good_name_events,
      miseventos = miseventos,
      mitabla = mitabla,
      Pvalues = Pvalues,
      # surv = surv,
      deltaPSI = deltaPSI,
      event_names2 = event_names2,
      mitabla_2 = mitabla_2,
      mitabla3 = mitabla3,
      nombre_tipo_cancer = input$tipodecancer,
      ExS = ExS,
      SG_list =SG_list,
      infoevents =infoevents
    )
    
  })
  
  ####### **** PANEL 1 ***** #######
  ####### SHOW Tabla 1 #######
  
  output$foo <- DT::renderDataTable(
    
    Run()$mitabla_2, escape = FALSE, selection = 'single', server = FALSE, rownames = FALSE,
    options = list(dom = 'ft', autoWidth=TRUE, paging = FALSE, scrollX = FALSE , 
                   scrollY = "300px", ordering = FALSE,
                   targets="_all",columnDefs = list(list(className = 'dt-center',width='auto', targets = "_all")), initComplete = JS(
      "function(settings, json) {",
      "$(this.api().table().header()).css({'background-color': '#20a6a6', 'color': '#fff', 'text-align': 'center','font-size':'12px'});",
      "$(this.api().table().body()).css({'background-color': '#fff','outline-color': '#fff','color': 'grey','text-align': 'center','font-size':'12px'});",
      "row.css({'background-color': '#20a6a6'})",
      "$('#example').DataTable( {fixedHeader: true} )",
      "$('#myCollapsible').on('shown.bs.collapse', function () {$($.fn.dataTable.tables(true)).DataTable().columns.adjust();});",
      
      "}"
    )
    )
    
  )
  
  ####### DOWNLOAD Tabla 1 #######
  output$download_tabla1 <-downloadHandler(
    # filename = function(){paste0('RBP table related to ',input$tipodecancer,'.xlsx')},
    filename = function(){paste0('RBP table related to ',Run()$nombre_tipo_cancer,'.xlsx')},
    content = function(file){
      # browser()
      if(input$updateresults == 0){
        mitabla <- rbind(rep("",7))
        colnames(mitabla) <- c(" ","Type","NHits","Pvalues ","X","QH","FC")
        mitabla <- as.data.frame(mitabla)
        write.xlsx(mitabla,
                   file)
      }else{
        
        mitabla <- Run()$mitabla_2
        # browser()
        mitabla <- cbind(rownames(mitabla),mitabla[,c(3:ncol(mitabla))])
        colnames(mitabla)[1] <- "RBP"
        write.xlsx(mitabla,
                   file)
      }
    }
  )
  
  
  
  ####### SHOW Tabla 2 #######
  observeEvent(input$tabla1,{
    # browser()
    num_ch <- substr(input$tabla1, start = 8, stop = 8)
    num_fila <- as.numeric(num_ch)
    
    rbp_seleccionado <- Run()$mitabla$RBP[num_fila]
    pos_eventos_seleccionados <- which(Run()$ExS[,rbp_seleccionado]==1)
    eventos_seleccionados <- names(pos_eventos_seleccionados)
    
    # tablaCancer <-cbind(Run()$Pvalues,Run()$deltaPSI)
    tablaCancer <-Run()$mitabla3
    head(tablaCancer)
    # colnames(tablaCancer)<-c("Pvalues","deltaPSI")
    
    #ordenamos
    # tablaCancer <- as.data.frame(tablaCancer)
    # tablaCancer <- tablaCancer[order(tablaCancer$Pvalues), ]
    
    # tablaCancer <- as.data.frame(tablaCancer)
    
    #conversion tablaCancer - tablaRBP
    tablaRBP <- tablaCancer[eventos_seleccionados,]
    
    #ranking
    position_juan <- match(eventos_seleccionados,rownames(tablaCancer))
    
    #borramos columna de eventos
    tablaRBP <- cbind(tablaRBP,position_juan)
    # colnames(tablaRBP) <- c("Pvalues", "deltaPSI", "ranking")
    colnames(tablaRBP)[ncol(tablaRBP)] <- "ranking"
    row_sub = apply(tablaRBP, 1, function(row) all(row !=0 ))
    tablaRBP <- tablaRBP[row_sub,]
    tablaRBP <- na.omit(tablaRBP)
    # head(tablaRBP)
    #ordenamos
    tablaRBP_ordenados <- tablaRBP[order(tablaRBP$Pvalues), ]
    
    #tablaRBP <- as.data.frame(tablaRBP)
    
    tablaRBP_ordenados$Pvalues<- format(tablaRBP_ordenados$Pvalues, scientific = TRUE)
    tablaRBP_ordenados$deltaPSI<- format(tablaRBP_ordenados$deltaPSI, scientific = TRUE)
    
    #cambiar nombres de eventos
    # eventos_finales <- rownames(tablaRBP_ordenados)
    # gn_eventos <- Run()$event_names2[eventos_finales,2]
    # tablaRBP_ordenados<-cbind(gn_eventos,tablaRBP_ordenados)
    tablaRBP_ordenados <- tablaRBP_ordenados[,c(1,5,6,3,4,7)]
    colnames(tablaRBP_ordenados)[2] <- c("Pick")
    
    output$foo_3 = DT::renderDataTable(
      tablaRBP_ordenados, escape = FALSE, selection = 'single', server = FALSE, rownames=FALSE,
      options = list(dom = 'ft', paging = FALSE,autoWidth = TRUE,scrollX = FALSE, scrollY = "300px", ordering = TRUE,targets="_all",
                     columnDefs = list(list(className = 'dt-center',width='auto', targets = "_all")), initComplete = JS(
        "function(settings, json) {",
        "$(this.api().table().header()).css({'background-color': '#20a6a6', 'color': '#fff','text-align': 'center'});",
        "$(this.api().table().body()).css({'background-color': '#fff','outline-color': '#fff','color': 'grey','text-align': 'center','font-size':'13px'});",
        "row.css({'background-color': '#20a6a6'})",
        "$('#example').DataTable( {fixedHeader: true} )",
        "$('#myCollapsible').on('shown.bs.collapse', function () {$($.fn.dataTable.tables(true)).DataTable().columns.adjust();});",
        "}"))
    )
    
    # DT::renderDataTable(
    #   Run()$mitabla_2, escape = FALSE, selection = 'single', server = FALSE, rownames = FALSE,
    #   options = list(dom = 'ft', autoWidth=TRUE, paging = FALSE, scrollX = FALSE , 
    #                  scrollY = "300px", ordering = FALSE,
    #                  targets="_all",columnDefs = list(list(className = 'dt-center',width='auto', targets = "_all")), initComplete = JS(
    #                    "function(settings, json) {",
    #                    "$(this.api().table().header()).css({'background-color': '#C5CAE9', 'color': '#fff', 'text-align': 'center','font-size':'12px'});",
    #                    "$(this.api().table().body()).css({'background-color': '#fff','outline-color': '#fff','color': 'grey','text-align': 'center','font-size':'12px'});",
    #                    "row.css({'background-color': '#C5CAE9'})",
    #                    "$('#example').DataTable( {fixedHeader: true} )",
    #                    "$('#myCollapsible').on('shown.bs.collapse', function () {$($.fn.dataTable.tables(true)).DataTable().columns.adjust();});",
    #                    "}"
    #                  )
    #   )
    # )
    
  })
  ####### DOWNLOAD Tabla 2 #######
  output$download_tablaCancer <-downloadHandler(
    filename = function(){paste0('Events.xlsx')},
    content = function(file) {
      if(input$updateresults == 0){
        mitabla <- rbind(rep("",7))
        colnames(mitabla) <- c(" ","Type","NHits","Pvalues ","X","QH","FC")
        mitabla <- as.data.frame(mitabla)
        write.xlsx(mitabla,
                   file)
      }else{
        num_ch <- substr(input$tabla1, start = 8, stop = 8)
        num_fila <- as.numeric(num_ch)
        
        rbp_seleccionado <- Run()$mitabla$RBP[num_fila]
        pos_eventos_seleccionados <- which(Run()$ExS[,rbp_seleccionado]==1)
        eventos_seleccionados <- names(pos_eventos_seleccionados)
        
        tablaCancer <-cbind(Run()$Pvalues,Run()$deltaPSI)
        colnames(tablaCancer)<-c("Pvalues","deltaPSI")
        
        #ordenamos
        tablaCancer <- as.data.frame(tablaCancer)
        tablaCancer <- tablaCancer[order(tablaCancer$Pvalues), ]
        
        tablaCancer <- as.data.frame(tablaCancer)
        
        #conversion tablaCancer - tablaRBP
        tablaRBP <- tablaCancer[eventos_seleccionados,]
        
        #ranking
        position_juan <- match(eventos_seleccionados,rownames(tablaCancer))
        
        #borramos columna de eventos
        tablaRBP <- cbind(tablaRBP,position_juan)
        colnames(tablaRBP) <- c("Pvalues", "deltaPSI", "ranking")
        
        row_sub = apply(tablaRBP, 1, function(row) all(row !=0 ))
        tablaRBP <- tablaRBP[row_sub,]
        tablaRBP <- na.omit(tablaRBP)
        
        #ordenamos
        tablaRBP_ordenados <- tablaRBP[order(tablaRBP$Pvalues), ]
        
        #tablaRBP <- as.data.frame(tablaRBP)
        
        tablaRBP_ordenados$Pvalues<- format(tablaRBP_ordenados$Pvalues, scientific = TRUE)
        tablaRBP_ordenados$deltaPSI<- format(tablaRBP_ordenados$deltaPSI, scientific = TRUE)
        
        #cambiar nombres de eventos
        eventos_finales <- rownames(tablaRBP_ordenados)
        gn_eventos <- Run()$event_names2[eventos_finales,2]
        tablaRBP_ordenados<-cbind(gn_eventos,tablaRBP_ordenados)
        colnames(tablaRBP_ordenados) <- c(" ","Pvalues", "deltaPSI", "ranking")
        write.xlsx(tablaRBP_ordenados,
                   file)
      }
    })
  #     
  #           
  #     
  
  ###### PLOT event ###########
  
  # observeEvent(input$tablaCancer,{
  output$plot_panel1 <- renderPlot({
    # browser()
    if (is.null(input$tablaCancer)) {
      
    }else{
    mievent_id <- input$tablaCancer
    mitabla3 <- Run()$mitabla3
    mievent_2 <- rownames(mitabla3)[match(mievent_id,mitabla3$Event)]
    
    SG_list <- Run()$SG_list
    infoevents <- Run()$infoevents
    
    midibujo <- draw_event(mievent = mievent_2,SG_list = SG_list,
                           infoevents = infoevents,good_anme=mievent_id)
    
    print(midibujo)
    
    # browser()
    cancer <- input$tipodecancer
    nombre_evento_bueno <- input$tablaCancer
    ###### Download plot ######
    output$download_plot_panel1 <-
      downloadHandler(
        filename = paste0("Plot ",cancer," evento ",nombre_evento_bueno,".png"),
        content = function(file){
          # ggsave(file,plot=p,scale=1.6)
          ggsave(file,plot=midibujo,units = "cm",width = 30,height = 15)
        })
    
    }
    
  })
  
  
  ####### *******  PANEL 2 ********** #######
  ####### SHOW TABLE OF EVENTS #######
  
  output$foo_2 = DT::renderDataTable(
    Run()$mitabla3[1:100,-5],escape = FALSE, selection = 'single', server = FALSE, rownames=FALSE,
    options = list(dom = 'ft',autoWidth=TRUE, scrollX = FALSE, scrollY = "600px",paging = FALSE, ordering = TRUE,targets="_all",
                   columnDefs = list(list(className = 'dt-center',width='auto', targets = "_all")),initComplete = JS(
      "function(settings, json) {",
      "$(this.api().table().header()).css({'background-color': '#20a6a6', 'color': '#fff', 'text-align': 'center','font-size':'13px'});",
      "$(this.api().table().body()).css({'background-color': '#fff','outline-color': '#fff','color': 'grey','text-align': 'center','font-size':'13px'});",
      "row.css({'background-color': '#20a6a6'})",
      "$('#example').DataTable( {fixedHeader: true} )",
      "$('#myCollapsible').on('shown.bs.collapse', function () {$($.fn.dataTable.tables(true)).DataTable().columns.adjust();});",
      "}"
    )
    )
  )
  
  
  
  
  ###### Download Tabla panel 2 ######
  
  output$download_tabla_panel2 <-downloadHandler(
    filename = function(){paste0('Events table related to ',Run()$nombre_tipo_cancer,'.xlsx')},
    # filename = function(){paste0('Events table related.xlsx')},
    content = function(file){
      # browser()
      if(input$updateresults == 0){
        mitabla <- rbind(rep("",7))
        colnames(mitabla) <- c(" ","Type","NHits","Pvalues ","X","QH","FC")
        mitabla <- as.data.frame(mitabla)
        write.xlsx(mitabla,
                   file)
      }else{
        # browser()
        mitabla3 <- Run()$mitabla3[ , c(1,3,4)]
        colnames(mitabla3)<-c("EventName","Pvalue","deltaPSI")
        write.xlsx(mitabla3,file)
      }
    }
  )
  outputOptions(output, "download_tabla_panel2", suspendWhenHidden=FALSE)
  
  
  #   
  ####### Plot de path #######
  output$plot <- renderPlot({
    # browser()
    if (is.null(input$tabla2)) {
      
    }else{
      mievent_id <- input$tabla2
      mitabla3 <- Run()$mitabla3
      mievent_2 <- rownames(mitabla3)[match(mievent_id,mitabla3$Event)]
      
      SG_list <- Run()$SG_list
      infoevents <- Run()$infoevents
      
      midibujo <- draw_event(mievent = mievent_2,SG_list = SG_list,
                             infoevents = infoevents,good_anme=mievent_id)
      
      print(midibujo)
      
      # browser()
      cancer <- input$tipodecancer
      nombre_evento_bueno <- input$tabla2
      ###### Download plot ######
      output$download_plot <-
        downloadHandler(
          filename = paste0("Plot ",cancer," evento ",nombre_evento_bueno,".png"),
          content = function(file){
            # ggsave(file,plot=p,scale=1.6)
            ggsave(file,plot=midibujo,units = "cm",width = 30,height = 15)
          })
      
    }
    
  })
  #   

}

shinyApp(ui = ui, server = server)

